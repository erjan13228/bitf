public class BubbleSort {
	public static void bubbleSort(int[] list) {
		boolean nextPass = true;

		for(int k = 1; k < list.length && nextPass; k++){
			nextPass = false;
			for(int i = 0; i < list.length - k; i++){
				if(list[i] > list[i + 1]){
					int temp = list[i];
					list[i] = list[i + 1];
					list[i + 1] = temp;

					nextPass = true;
				}
			}
		}
	}
	public static void main(String[] args){
		int[] list = {78, 1, 37, 12, 43};
		bubbleSort(list);
		for(int i = 0; i < list.length; i++)
			System.out.print(list[i] + " ");
	}
}